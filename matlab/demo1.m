clear all;
close all;

A = readtable("../PrecodingCalculationLib/DemoApplication/result.csv");
Nrows = length(A.size);
Nstrats = 9;
Nperstrat = Nrows / Nstrats;

avginits = zeros(Nstrats, Nperstrat);
avgsteps = zeros(Nstrats, Nperstrat);
sizes = zeros(Nperstrat, 1);

for i = 1:Nrows
    j = i;
    
    if i <= Nperstrat
        sizes(i) = A.size(i);
    else
        j = rem(i - 1, Nperstrat) + 1;
    end
    
    avginits(A.strategy_id(i), j) = A.sum_inits(i) / A.inits(i);
    avgsteps(A.strategy_id(i), j) = A.sum_steps(i) / A.steps(i);
end

subplot(1,2,1);
bar(avginits), grid on;
set(gca, 'YScale', 'log');
% set(gca, 'XTickLabel', sizes);

subplot(1,2,2);
bar(avgsteps), grid on;
set(gca, 'YScale', 'log');
% set(gca, 'XTickLabel', sizes);