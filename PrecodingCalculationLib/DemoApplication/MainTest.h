#pragma once

#include <iostream>
#include <fstream>
#include <chrono>
#include "PrecodingCalculationLib.h"
#include "StrategyTest.h"

using namespace std;
using namespace PrecodingCalculationLib;

class MainTest
{
private:
	string _outputFilePath;
	ofstream _outputFile;

	StrategyTest *_getStrategyTestByID(unsigned int id);
	void _putFileHeader();
	void _saveDataToFile(
		unsigned int strategy_id,
		unsigned int size,
		unsigned int inits,
		unsigned int steps,
		double sum_inits,
		double sum_steps
	);

	unsigned int _testCount = 9;
	/*/
	unsigned int _testSizesCount = 7;
	unsigned int _testSizes[7] = { 3, 5, 10, 50, 100, 250, 500 };
	/*/
	unsigned int _testSizesCount = 3;
	unsigned int _testSizes[3] = { 10, 20, 50 };
	//*/
	unsigned int _testInits = 10;
	unsigned int _testStepsPerInit = 60;


public:
	MainTest();
	~MainTest();
	
	void Perform();
	void Tests();
};
