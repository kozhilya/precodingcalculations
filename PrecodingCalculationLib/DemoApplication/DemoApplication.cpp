﻿#include "pch.h"

#include <iostream>
#include "PrecodingCalculationLib.h"

#include "BasicDemonstrations.h"
#include "StrategyDemonstration.h"
#include "StrategyTest.h"
#include "MainTest.h"

using namespace std;
using namespace PrecodingCalculationLib;

void _PerformTest(unsigned int menu, unsigned int cmd)
{
	switch (menu)
	{
	case 1:
		switch (cmd)
		{
		case 1:
			cout << "===== LU-разложение матрицы методом Гаусса =====" << endl;
			test_LU_Gauss();
			break;

		case 2:
			cout << "===== LU-разложение матрицы методом Жордана =====" << endl;
			test_LU_Jordan();
			break;

		case 3:
			cout << "===== Решение СЛАУ методом Гаусса =====" << endl;
			test_Solve();
			break;

		case 4:
			cout << "===== Обращение матрицы после её LU-разложение методом Жордана =====" << endl;
			test_Inverse_LU();
			break;

		case 5:
			cout << "===== Элиминативное обращение матрицы после её LU-разложение методом Гаусса =====" << endl;
			test_Inverse_LU_Eliminative();
			break;

		case 6:
			cout << "===== Решение СЛАУ через LU-разложение методом Жордана =====" << endl;
			test_Solve_LU();
			break;

		case 7:
			cout << "===== QR-разложение по схеме Хаусхолдера =====" << endl;
			test_QR();
			break;

		case 8:
			cout << "===== Решение СЛАУ через QR-разложение по схеме Хаусхолдера =====" << endl;
			test_Solve_QR();
			break;

		case 9:
			cout << "===== МНК-решение СЛАУ по схеме Поттера =====" << endl;
			test_Solve_LS();
			break;
		}
		break;

	case 2:
		switch (cmd)
		{
		case 1:
			cout << "===== Стратегия вычисления обратной матрицы после LU-разложения по схеме Гаусса =====" << endl;
			test_Strategy(new PrecodingStrategy1());
			break;

		case 2:
			cout << "===== Стратегия вычисления обратной матрицы после LU-разложения по схеме Жордана =====" << endl;
			test_Strategy(new PrecodingStrategy2());
			break;

		case 3:
			cout << "===== Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Жордана =====" << endl;
			test_Strategy(new PrecodingStrategy3());
			break;

		case 4:
			cout << "===== Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Гаусса =====" << endl;
			test_Strategy(new PrecodingStrategy4());
			break;

		case 5:
			cout << "===== Стратегия LU-разложения по схеме Жордана и решение систем без обратной матрицы =====" << endl;
			test_Strategy(new PrecodingStrategy5());
			break;

		case 6:
			cout << "===== Стратегия LU-разложение по схеме Гаусса и решение систем без обратной матрицы =====" << endl;
			test_Strategy(new PrecodingStrategy6());
			break;

		case 7:
			cout << "===== Стратегия QR-разложение по схеме Хаусхолдера и решение систем без обратной матрицы =====" << endl;
			test_Strategy(new PrecodingStrategy7());
			break;

		case 8:
			cout << "===== Стратегия последовательного МНК-решения систем по схеме Поттера без обратной матрицы =====" << endl;
			test_Strategy(new PrecodingStrategy8());
			break;

		case 9:
			cout << "===== Стратегия упрощенный линейный прекодер (теорема Гершгорина) =====" << endl;
			test_Strategy(new PrecodingStrategy9());
			break;


		}
		break;

	case 3:
		cout << "===== Базовое тестирование стратегии =====" << endl;
		SimpleStrategyTest *test;

		switch (cmd)
		{
		case 1:
			cout << "Стратегия: Вычисление обратной матрицы после LU-разложения по схеме Гаусса" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy1());
			break;
			
		case 2:
			cout << "Стратегия: Вычисление обратной матрицы после LU-разложения по схеме Жордана" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy2());
			break;

		case 3:
			cout << "Стратегия: Элиминативное вычисление обратной матрицы после LU-разложения по схеме Жордана" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy3());
			break;

		case 4:
			cout << "Стратегия: Элиминативное вычисление обратной матрицы после LU-разложения по схеме Гаусса" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy4());
			break;

		case 5:
			cout << "Стратегия: LU-разложение по схеме Жордана и решение систем без обратной матрицы" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy5());
			break;

		case 6:
			cout << "Стратегия: LU-разложение по схеме Гаусса и решение систем без обратной матрицы" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy6());
			break;

		case 7:
			cout << "Стратегия: QR-разложение по схеме Хаусхолдера и решение систем без обратной матрицы" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy7());
			break;

		case 8:
			cout << "Стратегия: Последовательное МНК-решение систем по схеме Поттера без обратной матрицы" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy8());
			break;

		case 9:
			cout << "Стратегия: Упрощенный линейный прекодер (теорема Гершгорина)" << endl;
			test_SimpleStrategyTest(new PrecodingStrategy9());
			break;

		default:
			return;
		}
		
		break;

	case 4:
		MainTest *mainTest;

		switch (cmd)
		{
		case 1:
			mainTest = new MainTest();

			mainTest->Perform();

			break;

		default:
			break;
		}
		break;
	}
}

void _PrintMenu(unsigned int menu) {

	switch (menu)
	{
	default:
	case 0: // Main menu
		cout << "===== Меню [Главное] =====" << endl << endl;

		cout << "  1. Демонстрация работы базовых функций" << endl;
		cout << "  2. Демонстрация работы методов прекодинга" << endl;
		cout << "  3. Базовое тестирование стратегии" << endl;
		cout << "  4. Основное тестирование";

		cout << endl << "  0. Выйти из программы" << endl;
		break;

	case 1: 
		cout << "===== Меню [Демонстрация работы базовых функций] =====" << endl << endl;

		cout << "  1. LU-разложение матрицы методом Гаусса" << endl;
		cout << "  2. LU-разложение матрицы методом Жордана" << endl;
		cout << "  3. Решение СЛАУ методом Гаусса" << endl;
		cout << "  4. Обращение матрицы после её LU-разложение методом Жордана (*)" << endl;
		cout << "  5. Элиминативное обращение матрицы после её LU-разложение методом Гаусса (*)" << endl;
		cout << "  6. Решение СЛАУ через LU-разложение методом Жордана (*)" << endl;
		cout << "  7. QR-разложение по схеме Хаусхолдера" << endl;
		cout << "  8. Решение СЛАУ через QR-разложение по схеме Хаусхолдера" << endl;
		cout << "  9. МНК-решение СЛАУ по схеме Поттера" << endl;

		cout << endl << "  0. <<< Вернуться в главное меню" << endl;

		cout << endl << endl << "(*) Разложения Гаусса и Жордана выдают одинковый результат, так что выбор метода разложения не влияет на результат здесь." << endl;
		break;

	case 2:
		cout << "===== Меню [Демонстрация работы методов прекодинга] =====" << endl << endl;

		cout << "  1. Стратегия вычисления обратной матрицы после LU-разложения по схеме Гаусса" << endl;
		cout << "  2. Стратегия вычисления обратной матрицы после LU-разложения по схеме Жордана" << endl;
		cout << "  3. Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Жордана" << endl;
		cout << "  4. Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Гаусса" << endl;
		cout << "  5. Стратегия LU-разложения по схеме Жордана и решение систем без обратной матрицы" << endl;
		cout << "  6. Стратегия LU-разложение по схеме Гаусса и решение систем без обратной матрицы" << endl;
		cout << "  7. Стратегия QR-разложение по схеме Хаусхолдера и решение систем без обратной матрицы" << endl;
		cout << "  8. Стратегия последовательного МНК-решения систем по схеме Поттера без обратной матрицы" << endl;
		cout << "  9. Стратегия упрощенный линейный прекодер (теорема Гершгорина)" << endl;

		cout << endl << "  0. <<< Вернуться в главное меню" << endl;

		break;

	case 3:
		cout << "===== Меню [Базовое тестирование стратегии] =====" << endl << endl;

		cout << "  1. Стратегия вычисления обратной матрицы после LU-разложения по схеме Гаусса" << endl;
		cout << "  2. Стратегия вычисления обратной матрицы после LU-разложения по схеме Жордана" << endl;
		cout << "  3. Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Жордана" << endl;
		cout << "  4. Стратегия элиминативного вычисления обратной матрицы после LU-разложения по схеме Гаусса" << endl;
		cout << "  5. Стратегия LU-разложения по схеме Жордана и решение систем без обратной матрицы" << endl;
		cout << "  6. Стратегия LU-разложение по схеме Гаусса и решение систем без обратной матрицы" << endl;
		cout << "  7. Стратегия QR-разложение по схеме Хаусхолдера и решение систем без обратной матрицы" << endl;
		cout << "  8. Стратегия последовательного МНК-решения систем по схеме Поттера без обратной матрицы" << endl;
		cout << "  9. Стратегия упрощенный линейный прекодер (теорема Гершгорина)" << endl;

		cout << endl << "  0. <<< Вернуться в главное меню" << endl;

		break;


	case 4:
		cout << "===== Основное тестирование =====" << endl << endl;

		cout << "  1. Произвести тестирование" << endl;

		cout << endl << "  0. <<< Вернуться в главное меню" << endl;
		break;
	}

}

int main()
{
	setlocale(LC_ALL, "rus");

	bool exit = false;
	unsigned int menu = 0;
	unsigned int cmd = 0;

	while (!exit) 
	{
		system("CLS");
		if (cmd == 0) 
		{
			menu = 0;
		}
		else if (menu == 0)
		{
			menu = cmd;
		}
		else 
		{
			_PerformTest(menu, cmd);
		}

		_PrintMenu(menu);

		cout << endl << "> ";
		cin >> cmd;

		// If getting command 0 in Main menu - exit.
		if ((cmd == 0) && (menu == 0)) 
		{
			exit = true;
		}
	}

	return 0;
}

/* TODO
- Разобраться с virtual
*/
