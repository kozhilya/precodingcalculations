#include "pch.h"
#include "StrategyTest.h"


StrategyTest::StrategyTest(PrecodingStrategy *strategy)
{
	this->_strategy = strategy;
}


StrategyTest::~StrategyTest()
{
}

void StrategyTest::TimingFieldsInit(const unsigned int _count)
{
	this->_timingCount = 4;
	this->_timingTiteles = new std::string[this->_timingCount];
	this->timings = new duration_t[this->_timingCount];
}
void StrategyTest::Init()
{
	this->TimingFieldsInit(4);

	this->_timingTiteles[0] = "������������� �������";
	this->_timingTiteles[1] = "��������������� ���������� �������������";
	this->_timingTiteles[2] = "����� ����������������� ����� �������������";
	this->_timingTiteles[3] = "������� ����������������� ���� ����� �������������";
}
void StrategyTest::PreformTest()
{
	timepoint_t start, end; 
	duration_t longtest_duration;

	this->Init();

	start = std::chrono::steady_clock::now();
	this->CreateArray();
	end = std::chrono::steady_clock::now();
	this->timings[0] = end - start;

	start = std::chrono::steady_clock::now();
	this->StrategyInit();
	end = std::chrono::steady_clock::now();
	this->timings[1] = end - start;

	longtest_duration = start - start; // ���������

	for (unsigned int i = 0; i < _testDuration; i++)
	{
		this->StrategyStepPreparation();

		start = std::chrono::steady_clock::now();
		this->StrategyStep();
		end = std::chrono::steady_clock::now();

		longtest_duration += end - start;
	}

	this->timings[2] = longtest_duration;
	this->timings[3] = longtest_duration / _testDuration;
}

void StrategyTest::DumpResults()
{
	for (unsigned int i = 0; i < this->_timingCount; i++)
	{
		std::cout << "    " << this->_timingTiteles[i] << ": " << this->timings[i].count() << " ��" << std::endl;
	}
}



void SimpleStrategyTest::CreateArray()
{
	this->_H = ComplexMatrix_T(SimpleStrategyTest::N);

	unsigned int i, j;

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			double abs = 
				((double)rand() / RAND_MAX) 
					* ((i == j) ? (G_max - G_min) : (Q_max - Q_min))
					+ ((i == j) ? G_min : Q_min);

			this->_H(i, j) = Complex_T::Random(abs);
		}
	}
}

void SimpleStrategyTest::StrategyInit()
{
	this->_strategy->Init(this->_H);
}

void SimpleStrategyTest::StrategyStepPreparation()
{
	this->_X = ComplexMatrix_T(SimpleStrategyTest::N, 1);

	unsigned int i;

	for (i = 0; i < N; i++)
	{
		this->_X(i, 0) = Complex_T::Random();
	}
}

void SimpleStrategyTest::StrategyStep()
{
	this->_strategy->Step(this->_X);
}
