#pragma once

#include <iostream>
#include "PrecodingCalculationLib.h"

using namespace std;
using namespace PrecodingCalculationLib;

void test_LU_Gauss()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T L, U;

	A(0, 0) = 1; A(0, 1) = 3; A(0, 2) = 2;
	A(1, 0) = 1; A(1, 1) = 1; A(1, 2) = 4;
	A(2, 0) = 2; A(2, 1) = 2; A(2, 2) = 1;
	cout << "A: " << A << endl;

	PrecodingMatrxAlgorithms::LU_Gauss(A, L, U);
	cout << "L: " << L << endl;
	cout << "U: " << U << endl;

	cout << "ѕроверка (доложно быть нулевой матрицей): " << L * U - A << endl;
}
void test_LU_Jordan()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T L, U;

	A(0, 0) = 1; A(0, 1) = 3; A(0, 2) = 2;
	A(1, 0) = 1; A(1, 1) = 1; A(1, 2) = 4;
	A(2, 0) = 2; A(2, 1) = 2; A(2, 2) = 1;
	cout << "A: " << A << endl;
	
	PrecodingMatrxAlgorithms::LU_Jordan(A, L, U);
	cout << "L: " << L << endl;
	cout << "U: " << U << endl;

	cout << "ѕроверка (доложно быть нулевой матрицей): " << L * U - A << endl;
}
void test_Solve()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T B(3, 1);
	ComplexMatrix_T X;

	A(0, 0) = 1; A(0, 1) = 1; A(0, 2) = -3;
	A(1, 0) = 3; A(1, 1) = -2; A(1, 2) = 1;
	A(2, 0) = 2; A(2, 1) = 1; A(2, 2) = -2;
	cout << "A: " << A << endl;

	B(0, 0) = 2;
	B(1, 0) = -1;
	B(2, 0) = 0;
	cout << "B: " << B << endl;

	PrecodingMatrxAlgorithms::Solve_Gauss(A, B, X);
	cout << "X: " << X << endl;

	cout << "A: " << A << endl;
	cout << "B: " << B << endl;

	cout << "ѕроверка (доложно быть нулевым вектором): " << (A * X - B) << endl;
}
void test_Inverse_LU()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T L, U, Ainv;
	ComplexMatrix_T E = ComplexMatrix_T::Eye(3);

	A(0, 0) = 1; A(0, 1) = 3; A(0, 2) = 2;
	A(1, 0) = 1; A(1, 1) = 1; A(1, 2) = 4;
	A(2, 0) = 2; A(2, 1) = 2; A(2, 2) = 1;
	cout << "A: " << A << endl;

	PrecodingMatrxAlgorithms::LU_Jordan(A, L, U);
	cout << "L: " << L << endl;
	cout << "U: " << U << endl;

	cout << "ѕроверка LU-разложени¤ (доложно быть нулевой матрицей): " << (L * U - A) << endl;

	PrecodingMatrxAlgorithms::Solve_LU(L, U, E, Ainv);
	cout << "A^-1: " << Ainv << endl;

	cout << "ѕроверка обращени¤ (доложно быть нулевой матрицей): " << (Ainv * A - E) << endl;
}
void test_Inverse_LU_Eliminative()
{
	ComplexMatrix_T A(4);
	ComplexMatrix_T L, U, Ainv;

	A(0, 0) = 2; A(0, 1) = 4; A(0, 2) = -4; A(0, 3) = 6;
	A(1, 0) = 1; A(1, 1) = 4; A(1, 2) = 2; A(1, 3) = 1;
	A(2, 0) = 3; A(2, 1) = 8; A(2, 2) = 1; A(2, 3) = 1;
	A(3, 0) = 2; A(3, 1) = 5; A(3, 2) = 0; A(3, 3) = 5;
	cout << "A: " << A << endl;

	PrecodingMatrxAlgorithms::LU_Gauss(A, L, U);
	cout << "L: " << L << endl;
	cout << "U: " << U << endl;

	cout << "ѕроверка LU-разложени¤ (доложно быть нулевой матрицей): " << (L * U - A) << endl;

	PrecodingMatrxAlgorithms::Inverse_LU_Eliminative(L, U, Ainv);
	cout << "A^-1: " << Ainv << endl;

	cout << "ѕроверка обращени¤ (доложно быть нулевой матрицей): " << (Ainv * A - ComplexMatrix_T::Eye(4)) << endl;
}
void test_Solve_LU()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T L, U;
	ComplexMatrix_T B(3, 1);
	ComplexMatrix_T X;

	A(0, 0) = 1; A(0, 1) = 1; A(0, 2) = -3;
	A(1, 0) = 3; A(1, 1) = -2; A(1, 2) = 1;
	A(2, 0) = 2; A(2, 1) = 1; A(2, 2) = -2;
	cout << "A: " << A << endl;
	
	PrecodingMatrxAlgorithms::LU_Jordan(A, L, U);
	cout << "L: " << L << endl;
	cout << "U: " << U << endl;

	cout << "ѕроверка LU-разложени¤ (доложно быть нулевой матрицей): " << (L * U - A) << endl;

	B(0, 0) = 2;
	B(1, 0) = -1;
	B(2, 0) = 0;
	cout << "B: " << B << endl;

	PrecodingMatrxAlgorithms::Solve_LU(L, U, B, X);
	cout << "X: " << X << endl;

	cout << "ѕроверка решени¤ (доложно быть нулевым вектором): " << (A * X - B) << endl;
}
void test_QR()
{
	ComplexMatrix_T A(3, 3);
	ComplexMatrix_T Q, R;

	A(0, 0) = 3; A(0, 1) = 2; A(0, 2) = 1;
	A(1, 0) = 1; A(1, 1) = 1; A(1, 2) = 2;
	A(2, 0) = 2; A(2, 1) = 1; A(2, 2) = 3;
	cout << "A: " << A << endl;

	PrecodingMatrxAlgorithms::QR_Householder(A, Q, R);
	cout << "Q: " << Q << endl;
	cout << "R: " << R << endl;

	cout << "ѕроверка QR-разложени¤ (доложно быть нулевой матрицей): " << (Q * R - A) << endl;
	cout << "ѕроверка ортогональности Q (доложно быть нулевой матрицей): " << (Q * Q.Conjugate() - ComplexMatrix_T::Eye(3)) << endl;
}
void test_Solve_QR()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T Q, R;
	ComplexMatrix_T B(3, 1);
	ComplexMatrix_T X;

	A(0, 0) = 1; A(0, 1) = 1; A(0, 2) = -3;
	A(1, 0) = 3; A(1, 1) = -2; A(1, 2) = 1;
	A(2, 0) = 2; A(2, 1) = 1; A(2, 2) = -2;
	cout << "A: " << A << endl;

	PrecodingMatrxAlgorithms::QR_Householder(A, Q, R);
	cout << "Q: " << Q << endl;
	cout << "R: " << R << endl;

	cout << "ѕроверка QR-разложени¤ (доложно быть нулевой матрицей): " << (Q * R - A) << endl;
	cout << "ѕроверка ортогональности Q (доложно быть нулевой матрицей): " << (Q * Q.Conjugate() - ComplexMatrix_T::Eye(3)) << endl;

	B(0, 0) = 2;
	B(1, 0) = -1;
	B(2, 0) = 0;
	cout << "B: " << B << endl;

	PrecodingMatrxAlgorithms::Solve_QR(Q, R, B, X);
	cout << "X: " << X << endl;

	cout << "ѕроверка решени¤ (доложно быть нулевым вектором): " << (A * X - B) << endl;
}
void test_Solve_LS()
{
	ComplexMatrix_T A(3);
	ComplexMatrix_T B(3, 1);
	ComplexMatrix_T X;

	A(0, 0) = 1; A(0, 1) = 1; A(0, 2) = -3;
	A(1, 0) = 3; A(1, 1) = -2; A(1, 2) = 1;
	A(2, 0) = 2; A(2, 1) = 1; A(2, 2) = -2;
	cout << "A: " << A << endl;

	B(0, 0) = 2;
	B(1, 0) = -1;
	B(2, 0) = 0;
	cout << "B: " << B << endl;

	PrecodingMatrxAlgorithms::Solve_LS(A, B, X);
	cout << "X: " << X << endl;

	cout << "ѕроверка решени¤ (доложно быть нулевым вектором): " << (A * X - B) << endl;
}