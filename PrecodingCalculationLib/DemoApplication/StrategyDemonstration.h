#pragma once

#include <iostream>
#include "PrecodingCalculationLib.h"
#include "StrategyTest.h"

using namespace std;
using namespace PrecodingCalculationLib;

void test_Strategy(PrecodingStrategy *strategy)
{
	unsigned int i, j,
		N = 5,
		R = 1e2;

	double
		ADiag = 1e2,
		ANondiag = 1e-1;

	ComplexMatrix_T A(N, N);
	ComplexMatrix_T X(N, 1);
	ComplexMatrix_T Y;
	
	for (i = 0; i < N; i++)
	{
		X(i, 0) = Complex_T::Random() * R;

		for (j = 0; j < N; j++)
		{
			A(i, j) = Complex_T::Random() * ((i == j) ? ADiag : ANondiag);
		}
	}

	cout << "A: " << A << endl;
	cout << "X: " << X << endl;

	strategy->Init(A);

	Y = strategy->Step(X);

	cout << "Result: " << Y << endl;
}

void test_SimpleStrategyTest(PrecodingStrategy *strategy)
{
	SimpleStrategyTest *test = new SimpleStrategyTest(strategy);

	cout << "������ �������: " << test->N << endl;
	cout << "���������� ����� �����: " << test->_testDuration << endl << endl;

	cout << "~ �������������... ";
	test->Init();
	cout << "������." << endl;

	cout << "~ ������������... ";
	test->PreformTest();
	cout << "������." << endl;

	cout << endl << "���������� �����: " << endl;
	test->DumpResults();

	cout << endl;
}
