#pragma once

#include <iostream>
#include <chrono>
#include "PrecodingCalculationLib.h"

using namespace std;
using namespace PrecodingCalculationLib;

typedef std::chrono::time_point< std::chrono::steady_clock> timepoint_t;
typedef std::chrono::duration<double, std::milli> duration_t;

class StrategyTest
{
protected:
	std::string _testTitle;
	PrecodingStrategy *_strategy;

	std::string *_timingTiteles;

public:
	unsigned int _timingCount;
	duration_t *timings;

	unsigned int _testDuration = 100;

	StrategyTest(PrecodingStrategy *strategy);
	~StrategyTest();

	virtual void TimingFieldsInit(const unsigned int _count);
	virtual void Init();
	virtual void PreformTest();
	virtual void DumpResults();

	virtual void CreateArray() {}
	virtual void StrategyInit() {}
	virtual void StrategyStepPreparation() {}
	virtual void StrategyStep() {}
};


class SimpleStrategyTest : public StrategyTest
{
protected:
	ComplexMatrix_T _H;
	ComplexMatrix_T _X;

public:
	unsigned int N = 50;

	double G_min = 50;
	double G_max = 100;
	double Q_min = 0;
	double Q_max = 1;

	SimpleStrategyTest(PrecodingStrategy *strategy) : StrategyTest(strategy) {}

	void CreateArray();
	void StrategyInit();
	void StrategyStepPreparation();
	void StrategyStep();
};