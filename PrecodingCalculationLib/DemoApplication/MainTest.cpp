﻿#pragma once

#include "pch.h"
#include "MainTest.h"

StrategyTest *MainTest::_getStrategyTestByID(unsigned int id)
{
	PrecodingStrategy *_strategy;

	switch (id)
	{
	case 1:
		_strategy = new PrecodingStrategy1();
		break;

	case 2:
		_strategy = new PrecodingStrategy2();
		break;

	case 3:
		_strategy = new PrecodingStrategy3();
		break;

	case 4:
		_strategy = new PrecodingStrategy4();
		break;

	case 5:
		_strategy = new PrecodingStrategy5();
		break;

	case 6:
		_strategy = new PrecodingStrategy6();
		break;

	case 7:
		_strategy = new PrecodingStrategy7();
		break;

	case 8:
		_strategy = new PrecodingStrategy8();
		break;

	case 9:
		_strategy = new PrecodingStrategy9();
		break;

	default:
		return NULL;
	}

	return new SimpleStrategyTest(_strategy);
}

void MainTest::_putFileHeader()
{
	this->_outputFile << "strategy_id,size,inits,steps,sum_inits,sum_steps" << endl;
}

void MainTest::_saveDataToFile(unsigned int strategy_id, unsigned int size, unsigned int inits, unsigned int steps, double sum_inits, double sum_steps)
{
	this->_outputFile << strategy_id << "," << size << "," << inits << "," << steps << "," << sum_inits << "," << sum_steps << endl;
}

MainTest::MainTest()
{
}

MainTest::~MainTest()
{
}

void MainTest::Perform()
{
	cout << "===== Основное тестирование =====" << endl;

	/*
	cout << "Введите название файл-отчёта (.csv добавится автоматически)..." << endl;
	cin >> this->_outputFilePath;

	this->_outputFilePath += ".csv";
	/*/
	this->_outputFilePath = "result.csv";
	//*/

	this->_outputFile.open(this->_outputFilePath, ofstream::out | ofstream::trunc);
	this->_putFileHeader();

	cout << "Начинаю тестирование..." << endl;
	this->Tests();
	cout << "Тестирование завершено!" << endl;
	
	this->_outputFile.close();
}

void MainTest::Tests()
{
	unsigned int s, sizei, size, i;
	string prefix;
	duration_t sum_inits, sum_steps;

	for (s = 1; s <= this->_testCount; s++)
	{
		prefix = "[Стратегия #" + to_string(s) + "] ";

		cout << prefix << "Начинаю тестирование стратегии..." << endl;

		for (sizei = 0; sizei < this->_testSizesCount; sizei++)
		{
			size = this->_testSizes[sizei];

			cout << prefix << "Тестирование стратегии для N = " << size << 
				" (всего прогонов: " << this->_testInits << "): " << endl << "\t";

			sum_inits = duration_t(0);
			sum_steps = duration_t(0);

			for (i = 0; i < this->_testInits; i++)
			{
				SimpleStrategyTest *test = (SimpleStrategyTest *)this->_getStrategyTestByID(s);

				test->N = size;
				test->_testDuration = this->_testStepsPerInit;

				test->PreformTest();

				sum_inits += test->timings[1];
				sum_steps += test->timings[2];

				delete test;
				cout << i << "! ";
			}

			this->_saveDataToFile(s, size, this->_testInits, this->_testStepsPerInit, sum_inits.count(), sum_steps.count());
			cout << endl << prefix << "Готово, продолжаю работу..." << endl;
		}
	}

}
