#pragma once

#include "stdafx.h"

namespace PrecodingCalculationLib {

	/// <summary>
	/// Комплексная матрица
	/// </summary>
	extern "C" class ComplexMatrix_T
	{
	protected:
		/// <summary>
		/// Массив для хранения элементов матрицы.
		/// </summary>
		Complex_T* _data;

		/// <summary>
		/// Количество строк матрицы
		/// </summary>
		unsigned int _N;

		/// <summary>
		/// Количество столбцов матрицы
		/// </summary>
		unsigned int _M;

		/// <summary>
		/// Выделяет памяти для элементов матрицы
		/// </summary>
		void _alloc();

		/// <summary>
		/// Возвращает индекс элемента в массиве _data, который относится к элементу матрицы с заданными координатами.
		/// </summary>
		/// <exception cref="MatrixOutOfRangeException">Если искомый элемент выходит за границы матрицы.</exception>
		/// <param name="n">Строка искомого элемента.</param>
		/// <param name="m">Столбец искомого элемента.</param>
		/// <returns>Индекс искомого элемента.</returns>
		unsigned int _getIndex(unsigned int n, unsigned int m) const;

	public:
		/// <summary>
		/// Инициализирует <see cref="ComplexMatrix_T">комплексную матрицу</see> единичного размера.
		/// </summary>
		ComplexMatrix_T() : ComplexMatrix_T(1, 1) {}

		/// <summary>
		/// Инициализирует квадратную <see cref="ComplexMatrix_T">комплексную матрицу</see> заданного размера.
		/// </summary>
		/// <param name="N">Размерность матрицы</param>
		ComplexMatrix_T(unsigned int N) : ComplexMatrix_T(N, N) {}

		/// <summary>
		/// Инициализирует <see cref="ComplexMatrix_T">комплексную матрицу</see> заданных размеров.
		/// </summary>
		/// <param name="N">Количество строк матрицы</param>
		/// <param name="M">Количество столбцов</param>
		ComplexMatrix_T(unsigned int N, unsigned int M);

		/// <summary>
		/// Опеделяет, является ли матрица квадратной.
		/// </summary>
		/// <returns>
		///   <c>true</c>, если матрица является квадратной; иначе, <c>false</c>.
		/// </returns>
		bool IsSquare() const;

		/// <summary>
		/// Опеделяет, является ли матрица вектором (строкой или столбцом).
		/// </summary>
		/// <returns>
		///   <c>true</c>, если матрица является вектором; иначе, <c>false</c>.
		/// </returns>
		bool IsVector() const;

		/// <summary>
		/// Возвращает количество строк в матрице.
		/// </summary>
		/// <returns>Количество строк в матрице.</returns>
		unsigned int GetN() const;

		/// <summary>
		/// Возвращает количество столбцов в матрице.
		/// </summary>
		/// <returns>Количество столбцов в матрице.</returns>
		unsigned int GetM() const;

		/// <summary>
		/// Возвращает элемент матрицы.
		/// </summary>
		/// <param name="i">Строка искомого элемента.</param>
		/// <param name="j">Столбец искомого элемента.</param>
		/// <returns>Искомое значение элемента матрицы.</returns>
		Complex_T GetValue(unsigned int i, unsigned int j) const;

		/// <summary>
		/// Устанавливает новое знаение элемента матрицы.
		/// </summary>
		/// <param name="i">Строка изменяемого элемента.</param>
		/// <param name="j">Столбец изменяемого элемента.</param>
		/// <param name="value">Новое значение элемента матрицы.</param>
		void SetValue(unsigned int i, unsigned int j, const Complex_T value);

		/// <summary>
		/// Изменяет новое знаение элемента матрицы.
		/// </summary>
		/// <param name="i">Строка изменяемого элемента.</param>
		/// <param name="j">Столбец изменяемого элемента.</param>
		/// <returns>Объект элемента матрицы для изменения.</returns>
		Complex_T& operator() (unsigned int i, unsigned int j);

		/// <summary>
		/// Возвращает элемент матрицы.
		/// </summary>
		/// <param name="i">Строка искомого элемента.</param>
		/// <param name="j">Столбец искомого элемента.</param>
		/// <returns>Искомое значение элемента матрицы.</returns>
		Complex_T operator() (unsigned int i, unsigned int j) const;

		/// <summary>
		/// Транспонирует матрицу (не эрмитово-сопряжённую!)
		/// </summary>
		/// <returns>Транспонированная матрица.</returns>
		ComplexMatrix_T Transpose() const;

		/// <summary>
		/// Возвращает эрмитово-сопряжённую матрицу.
		/// </summary>
		/// <returns>Эрмитово-сопряжённая матрица.</returns>
		ComplexMatrix_T Conjugate() const;

		/// <summary>
		/// Вычисляет норму вектора, если матрица <see cref="ComplexMatrix_T::IsVector">является вектором</see>.
		/// </summary>
		/// <exception cref="MatrixNotAVectorException">Если матрица не является вектором.</exception>
		/// <returns>Норма вектора</returns>
		Complex_T Norm() const;

		/// <summary>
		/// Возвращает подматрицу.
		/// </summary>
		/// <param name="i">Номер первой строки подматрицы в исходной матрице.</param>
		/// <param name="j">Номер первого столбца подматрицы в исходной матрице.</param>
		/// <param name="height">Количество строк подматрицы.</param>
		/// <param name="width">Количество строк подматрицы.</param>
		/// <returns>Подматрица</returns>
		ComplexMatrix_T SubMatrix(unsigned int i, unsigned int j, unsigned int height, unsigned int width) const;

		~ComplexMatrix_T();

		/// <summary>
		/// Возвращает единичную квадратную матрицу заданного размера.
		/// </summary>
		/// <param name="n">Размерность матрицы.</param>
		/// <returns>Искомая единичная квадратная матрица.</returns>
		static ComplexMatrix_T Eye(unsigned int n);
	};

	extern std::ostream& operator<<(std::ostream&, const ComplexMatrix_T&);

	extern ComplexMatrix_T operator+(const ComplexMatrix_T);
	extern ComplexMatrix_T operator-(const ComplexMatrix_T);

	extern ComplexMatrix_T operator+(const ComplexMatrix_T, const ComplexMatrix_T);
	extern ComplexMatrix_T operator-(const ComplexMatrix_T, const ComplexMatrix_T);

	extern ComplexMatrix_T operator*(const ComplexMatrix_T, const ComplexMatrix_T);
	extern ComplexMatrix_T operator*(const double, const ComplexMatrix_T);
	extern ComplexMatrix_T operator*(const ComplexMatrix_T, const Complex_T);
	extern ComplexMatrix_T operator/(const ComplexMatrix_T, const Complex_T);
}