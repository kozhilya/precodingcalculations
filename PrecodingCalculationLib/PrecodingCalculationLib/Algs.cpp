#pragma once

#include "stdafx.h"

namespace PrecodingCalculationLib {

	void PrecodingMatrxAlgorithms::LU_Gauss(const ComplexMatrix_T A, ComplexMatrix_T& L, ComplexMatrix_T& U)
	{
		if (!A.IsSquare()) {
			throw new PrecodingStrategysNotSquareException();
		}

		unsigned int N = A.GetN(),
			i, j, k;

		ComplexMatrix_T B = +A; // Клонируем матрицу
		Complex_T _tmp;

		for (k = 0; k < N; k++) {
			// Вычитаем предыдущие строки
			for (i = 0; i < k; i++) {
				Complex_T koef = B(k, i);

				for (j = i + 1; j < N; j++) {
					B(k, j) = B(k, j) - koef * B(i, j);
				}
			}

			// Выбор главного элемента в строке 
			// (диагональный, так как нет понятия "максимальный элемент", когда мы в комплексной матрице.
			unsigned int lead = k;

			while ((lead < N) && (B(k, lead) == 0)) { lead++; } // Ищем справа элемент != 0
			if (lead == N) {
				// Дошли до края матрицы - выходим, унитарная матрица
				throw new PrecodingStrategysUnitaryException();
			}

			if (lead != k) {
				// Меняем столбцы местами
				for (j = 0; j < N; j++) {
					_tmp = B(j, lead);
					B(j, lead) = B(j, k);
					B(j, k) = _tmp;
				}
			}

			// Нормируем строку
			for (j = k + 1; j < N; j++) {
				B(k, j) = B(k, j) / B(k, k);
			}
		}

		L = ComplexMatrix_T(N);
		U = ComplexMatrix_T(N);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				if (i == j) {
					L(i, j) = B(i, j);
					U(i, j) = 1;
				}
				else if (i > j) {
					L(i, j) = B(i, j);
				}
				else if (i < j) {
					U(i, j) = B(i, j);
				}
			}
		}

	}

	void PrecodingMatrxAlgorithms::LU_Jordan(const ComplexMatrix_T A, ComplexMatrix_T &L, ComplexMatrix_T &U)
	{
		if (!A.IsSquare()) {
			throw new PrecodingStrategysNotSquareException();
		}

		unsigned int N = A.GetN(),
			i, j, k;

		ComplexMatrix_T B = +A; // Клонируем матрицу
		Complex_T _tmp;

		for (k = 0; k < N; k++) {
			// Выбор главного элемента в строке 
			// (диагональный, так как нет понятия "максимальный элемент", когда мы в комплексной матрице.
			unsigned int lead = k;

			while ((lead < N) && (B(k, lead) == 0)) { lead++; } // Ищем справа элемент != 0
			if (lead == N) {
				// Дошли до края матрицы - выходим, унитарная матрица
				throw new PrecodingStrategysUnitaryException();
			}

			if (lead != k) {
				// Меняем столбцы местами
				for (j = 0; j < N; j++) {
					_tmp = B(j, lead);
					B(j, lead) = B(j, k);
					B(j, k) = _tmp;
				}
			}

			// Нормируем первую строку матрицы
			for (j = k + 1; j < N; j++) {
				B(k, j) = B(k, j) / B(k, k);
			}

			// Вычитаем строки
			for (i = k; i < N; i++) {
				if (i == k) continue;

				Complex_T koef = B(i, k);

				for (j = k + 1; j < N; j++) {
					B(i, j) = B(i, j) - koef * B(k, j);
				}
			}
		}

		L = ComplexMatrix_T(N);
		U = ComplexMatrix_T(N);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				if (i == j) {
					L(i, j) = B(i, j);
					U(i, j) = 1;
				}
				else if (i > j) {
					L(i, j) = B(i, j);
				}
				else if (i < j) {
					U(i, j) = B(i, j); // Вообще, тут должно быть со знаком -, ибо вычисляется U^-1... Но ладно.
				}
			}
		}
	}

	void PrecodingMatrxAlgorithms::QR_Householder(const ComplexMatrix_T A, ComplexMatrix_T &Q, ComplexMatrix_T &R)
	{
		if (!A.IsSquare()) {
			throw new PrecodingStrategysNotSquareException();
		}

		unsigned int N = A.GetN(),
			i, j, k;

		Q = ComplexMatrix_T::Eye(N);
		R = +A;

		for (k = 0; k < N - 1; k++)
		{
			// Вычисляем вектор
			ComplexMatrix_T V = R.SubMatrix(k, k, N - k, 1);

			V(0, 0) = V(0, 0) - V.Norm();

			Complex_T _tmp = V.Norm();
			_tmp = Complex_T(2, 0) / (_tmp * _tmp);

			ComplexMatrix_T S = V * V.Conjugate() * _tmp;
			ComplexMatrix_T P = ComplexMatrix_T::Eye(N);

			for (i = 0; i < N - k; i++)
			{
				for (j = 0; j < N - k; j++)
				{
					P(i + k, j + k) = P(i + k, j + k) - S(i, j);
				}
			}

			Q = Q * P;
			R = P * R;

		}
	}

	void PrecodingMatrxAlgorithms::Solve_Gauss(const ComplexMatrix_T A, const ComplexMatrix_T B, ComplexMatrix_T& X)
	{
		if (!A.IsSquare() || (A.GetM() != B.GetN())) {
			throw new PrecodingStrategysDimensionMissmatchException();
		}

		unsigned int N = A.GetN(), M = B.GetM(),
			i, j, k, l;
		Complex_T _tmp, _tmp2;

		X = +B;

		ComplexMatrix_T a = +A;

		// Прямой ход
		for (i = 0; i < N; i++) {
			// Выбор главного элемента в столбце 
			unsigned int lead = i;

			while ((lead < N) && (a(lead, i) == 0)) { lead++; } // Ищем справа элемент != 0
			if (lead == N) {
				// Дошли до края матрицы - выходим, унитарная матрица
				throw new PrecodingStrategysUnitaryException();
			}

			if (lead != i) {
				// Меняем строки местами
				for (j = 0; j < N; j++) {
					_tmp = a(j, lead);
					a(j, lead) = a(j, i);
					a(j, i) = _tmp;
				}

				for (j = 0; j < M; j++) {
					_tmp = X(j, lead);
					X(j, lead) = X(j, i);
					X(j, i) = _tmp;
				}
			}

			// Нормализуем строку
			_tmp = a(i, i);
			for (j = 0; j < N; j++) {
				a(i, j) = a(i, j) / _tmp;
			}
			for (j = 0; j < M; j++) {
				X(i, j) = X(i, j) / _tmp;
			}

			// Вычитаем из следующих
			for (l = i + 1; l < N; l++) {
				_tmp = a(l, i);

				for (j = 0; j < N; j++) {
					a(l, j) = a(l, j) - a(i, j) * _tmp;
				}
				for (j = 0; j < M; j++) {
					X(l, j) = X(l, j) - X(i, j) * _tmp;
				}
			}
		}

		// Обратный ход
		for (i = N - 1; i != UINT_MAX; i--)
		{
			_tmp = a(i, i);

			for (j = 0; j < M; j++)
			{
				X(i, j) = X(i, j) / _tmp;
			}

			for (k = 0; k < i; k++)
			{
				_tmp2 = a(k, i);

				//*
				// Это действие необязательно для алгоритма, так что его можно опустить.
				a(k, i) = a(k, i) - a(i, i) * _tmp2;
				//*/

				for (j = 0; j < M; j++)
				{
					X(k, j) = X(k, j) - X(i, j) * _tmp2;
				}
			}
		}
	}

	void PrecodingMatrxAlgorithms::Solve_LS(const ComplexMatrix_T A, const ComplexMatrix_T B, ComplexMatrix_T& X)
	{
		ComplexMatrix_T L = A.Conjugate() * A;
		ComplexMatrix_T R = A.Conjugate() * B;

		Solve_Gauss(L, R, X);
	}

	void PrecodingMatrxAlgorithms::Solve_LU(const ComplexMatrix_T L, const ComplexMatrix_T U, const ComplexMatrix_T B, ComplexMatrix_T& X)
	{
		if (!L.IsSquare() || !U.IsSquare() || (L.GetM() != U.GetN())) {
			throw new PrecodingStrategysDimensionMissmatchException();
		}

		unsigned int N = L.GetN(), M = B.GetM(),
			i, j, k;
		Complex_T _tmp, _tmp2;

		X = +B;

		// L * Y = B (L: NxN, B: NxM => Y: NxM)
		ComplexMatrix_T l = +L;

		for (i = 0; i < N; i++)
		{
			_tmp = l(i, i);

			for (j = 0; j < N; j++)
			{
				l(i, j) = l(i, j) / _tmp;
			}

			for (j = 0; j < M; j++)
			{
				X(i, j) = X(i, j) / _tmp;
			}

			for (k = i + 1; k < N; k++)
			{
				_tmp2 = l(k, i);

				for (j = 0; j < N; j++)
				{
					l(k, j) = l(k, j) - l(i, j) * _tmp2;
				}

				for (j = 0; j < M; j++)
				{
					X(k, j) = X(k, j) - X(i, j) * _tmp2;
				}
			}
		}

		// U * X = Y (U: NxN, Y: NxM => X: NxM)
		ComplexMatrix_T u = +U;

		for (i = N - 1; i != UINT_MAX; i--)
		{
			// Вычитаем строки ниже
			for (k = i + 1; k < N; k++)
			{
				_tmp = u(i, k);

				for (j = 0; j < N; j++)
				{
					u(i, j) = u(i, j) - u(k, j) * _tmp;
				}

				for (j = 0; j < M; j++)
				{
					X(i, j) = X(i, j) - X(k, j) * _tmp;
				}
			}
		}
	}

	void PrecodingMatrxAlgorithms::Solve_QR(const ComplexMatrix_T Q, const ComplexMatrix_T R, const ComplexMatrix_T B, ComplexMatrix_T& X)
	{
		if (!Q.IsSquare() || !R.IsSquare() || (Q.GetM() != R.GetN())) {
			throw new PrecodingStrategysDimensionMissmatchException();
		}

		unsigned int N = Q.GetN(), M = B.GetM(),
			i, j, k;
		Complex_T _tmp, _tmp2;

		X = Q.Conjugate() * B;

		// U * X = Y (U: NxN, Y: NxM => X: NxM)
		ComplexMatrix_T r = +R;

		for (i = N - 1; i != UINT_MAX; i--)
		{
			_tmp = r(i, i);

			for (j = 0; j < M; j++)
			{
				X(i, j) = X(i, j) / _tmp;
			}

			for (k = 0; k < i; k++)
			{
				_tmp2 = r(k, i);

				/*
				// Это действие необязательно для алгоритма, так что его можно опустить.
				r(k, i) = r(k, i) - r(i, i) * _tmp2;
				//*/

				for (j = 0; j < M; j++)
				{
					X(k, j) = X(k, j) - X(i, j) * _tmp2;
				}
			}
		}
	}

	void PrecodingMatrxAlgorithms::Inverse_LU_Eliminative(const ComplexMatrix_T L, const ComplexMatrix_T U, ComplexMatrix_T& R)
	{
		if (!L.IsSquare() || !U.IsSquare() || (L.GetM() != U.GetN())) {
			throw new PrecodingStrategysDimensionMissmatchException();
		}

		unsigned int N = L.GetN(),
			i, j, k, l;

		ComplexMatrix_T E = ComplexMatrix_T::Eye(N);

		ComplexMatrix_T Linv = +E;
		ComplexMatrix_T Uinv = +E;
		ComplexMatrix_T _tmp;

		for (i = 0; i < N; i++) {
			_tmp = +E;

			_tmp(i, i) = _tmp(i, i) / L(i, i);

			for (j = i + 1; j < N; j++)
			{
				_tmp(j, i) = -L(j, i) / L(i, i);
			}

			Linv = _tmp * Linv;

			if (i > 0) {
				_tmp = +E;

				for (j = 0; j < i; j++)
				{
					_tmp(j, i) = -U(j, i);
				}

				Uinv = Uinv * _tmp;
			}
		}

		R = Uinv * Linv;
	}
}