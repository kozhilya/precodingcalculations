#pragma once

#include "stdafx.h"
#include <exception>

namespace PrecodingCalculationLib {

	extern "C" class MatrixException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Unknown Calc Lib SMatrix Exception";
		}
	};

	extern "C" class MatrixOutOfRangeException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SMatrix Exception: Out Of Range";
		}
	};

	extern "C" class MatrixDimensionMissmatchException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SMatrix Exception: Dimension Missmatch";
		}
	};

	extern "C" class MatrixNotAVectorException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SMatrix Exception: Not a Vector";
		}
	};

	extern "C" class ComplexDivisionByZeroException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SComplex_T Exception: Division By Zero";
		}
	};

	extern "C" class PrecodingStrategysNotSquareException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SPrecodingStrategys Exception: Matrix is not square";
		}
	};

	extern "C" class PrecodingStrategysUnitaryException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SPrecodingStrategys Exception: Matrix is unitary";
		}
	};

	extern "C" class PrecodingStrategysDimensionMissmatchException : public std::exception
	{
	public:
		const char * what() const throw ()
		{
			return "Calc Lib SPrecodingStrategys Exception: Dimension missmatch";
		}
	};

}