#pragma once

#include "stdafx.h"

namespace PrecodingCalculationLib {

	/// <summary>
	/// Комплексное число.
	/// </summary>
	extern "C" class Complex_T
	{
	public:
		/// <summary>
		/// Действительная часть комплексного числа.
		/// </summary>
		double Re;

		/// <summary>
		/// Мнимая часть комплексного числа.
		/// </summary>
		double Im;

		/// <summary>
		/// Инициализирует <see cref="Complex_T"/>, равное нулю.
		/// </summary>
		Complex_T() : Complex_T(0, 0) {}

		/// <summary>
		/// Инициализирует <see cref="Complex_T"/> с заданной действительной частью и нулевой мнимой частью.
		/// </summary>
		/// <param name="_Re">Действительная часть.</param>
		Complex_T(double _Re) : Complex_T(_Re, 0) {}

		/// <summary>
		/// Инициализирует <see cref="Complex_T"/> с заданными действительной и мнимой частью.
		/// </summary>
		/// <param name="_Re">Действительная часть.</param>
		/// <param name="_Im">Мнимая часть.</param>
		Complex_T(double _Re, double _Im);

		/// <summary>
		/// Вычисляет абсолютное значение комплекного числа.
		/// </summary>
		/// <returns>Абсолютное значение комплекного числа.</returns>
		double Abs() const;

		/// <summary>
		/// Вычисляет фазу комплекного числа.
		/// </summary>
		/// <returns>Фаза комплекного числа.</returns>
		double Angle() const;

		/// <summary>
		/// Возвращает сопряжённое комплексное число.
		/// </summary>
		/// <returns>Сопряжённое комплексное число.</returns>
		Complex_T Conjugate() const;

		/// <summary>
		/// Вычисляет квадратный корень комплексного числа.
		/// </summary>
		/// <returns>Квадратный корень комплексного числа.</returns>
		Complex_T Sqrt() const;

		/// <summary>
		/// Преобразует комплексное число в строковую запись.
		/// </summary>
		/// <returns>Строковая запись комплексного числа.</returns>
		std::string ToString() const;

		/// <summary>
		/// Finalizes an instance of the <see cref="Complex_T"/> class.
		/// </summary>
		~Complex_T();

		/// <summary>
		/// Генерирует случайное комплексное число в единичном квадрате.
		/// </summary>
		/// <returns>Случайнео комплекснео число.</returns>
		static Complex_T Random();

		/// <summary>
		/// Генерирует случайное комплексное число с заданным модулем.
		/// </summary>
		/// <param name="abs">Случайнео комплекснео число.</param>
		/// <returns></returns>
		static Complex_T Random(double abs);
	};

	extern std::ostream& operator<<(std::ostream&, const Complex_T&);

	extern bool operator==(const Complex_T, const Complex_T);
	extern bool operator!=(const Complex_T, const Complex_T);

	extern Complex_T operator+(const Complex_T);
	extern Complex_T operator-(const Complex_T);

	extern Complex_T operator+(const Complex_T, const Complex_T);
	extern Complex_T operator-(const Complex_T, const Complex_T);
	extern Complex_T operator*(const Complex_T, const Complex_T);
	extern Complex_T operator/(const Complex_T, const Complex_T);

}