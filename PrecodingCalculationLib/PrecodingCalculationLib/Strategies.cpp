#pragma once

#include "stdafx.h"

namespace PrecodingCalculationLib {

	ComplexMatrix_T PrecodingStrategy::HToA(ComplexMatrix_T H)
	{
		unsigned int N = H.GetN(), i;

		ComplexMatrix_T Di(N, N);
		for (i = 0; i < N; i++) {
			Di(i, i) = Complex_T(1, 0) / H(i, i);
		}

		return Di * H;
	}

	// PrecodingStrategy1
	void PrecodingStrategy1::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T L, U, A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Gauss(A, L, U);

		PrecodingMatrxAlgorithms::Solve_LU(L, U, ComplexMatrix_T::Eye(A.GetN()), this->_P_inv);
	}
	ComplexMatrix_T PrecodingStrategy1::Step(ComplexMatrix_T X)
	{
		return this->_P_inv * X;
	}

	// PrecodingStrategy2
	void PrecodingStrategy2::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T L, U, A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Jordan(A, L, U);

		PrecodingMatrxAlgorithms::Solve_LU(L, U, ComplexMatrix_T::Eye(A.GetN()), this->_P_inv);
	}
	ComplexMatrix_T PrecodingStrategy2::Step(ComplexMatrix_T X)
	{
		return this->_P_inv * X;
	}

	// PrecodingStrategy3
	void PrecodingStrategy3::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T L, U, A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Gauss(A, L, U);

		PrecodingMatrxAlgorithms::Inverse_LU_Eliminative(L, U, this->_P_inv);
	}
	ComplexMatrix_T PrecodingStrategy3::Step(ComplexMatrix_T X)
	{
		return this->_P_inv * X;
	}

	// PrecodingStrategy4
	void PrecodingStrategy4::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T L, U, A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Jordan(A, L, U);

		PrecodingMatrxAlgorithms::Inverse_LU_Eliminative(L, U, this->_P_inv);
	}
	ComplexMatrix_T PrecodingStrategy4::Step(ComplexMatrix_T X)
	{
		return this->_P_inv * X;
	}

	// PrecodingStrategy5
	void PrecodingStrategy5::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Gauss(A, this->_L, this->_U);
	}
	ComplexMatrix_T PrecodingStrategy5::Step(ComplexMatrix_T X)
	{
		ComplexMatrix_T result;

		PrecodingMatrxAlgorithms::Solve_LU(this->_L, this->_U, X, result);

		return result;
	}

	// PrecodingStrategy6
	void PrecodingStrategy6::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T A = this->HToA(H);
		PrecodingMatrxAlgorithms::LU_Jordan(A, this->_L, this->_U);
	}
	ComplexMatrix_T PrecodingStrategy6::Step(ComplexMatrix_T X)
	{
		ComplexMatrix_T result;

		PrecodingMatrxAlgorithms::Solve_LU(this->_L, this->_U, X, result);

		return result;
	}

	// PrecodingStrategy7
	void PrecodingStrategy7::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T A = this->HToA(H);
		PrecodingMatrxAlgorithms::QR_Householder(A, this->_Q, this->_R);
	}
	ComplexMatrix_T PrecodingStrategy7::Step(ComplexMatrix_T X)
	{
		ComplexMatrix_T result;

		PrecodingMatrxAlgorithms::Solve_QR(this->_Q, this->_R, X, result);

		return result;
	}

	// PrecodingStrategy8
	void PrecodingStrategy8::Init(ComplexMatrix_T H)
	{
		this->_A = this->HToA(H);
	}
	ComplexMatrix_T PrecodingStrategy8::Step(ComplexMatrix_T X)
	{
		ComplexMatrix_T result;

		PrecodingMatrxAlgorithms::Solve_LS(this->_A, X, result);

		return result;
	}

	// PrecodingStrategy9
	void PrecodingStrategy9::Init(ComplexMatrix_T H)
	{
		ComplexMatrix_T A = this->HToA(H);
		ComplexMatrix_T E = ComplexMatrix_T::Eye(H.GetN());

		this->_P_inv = E + E - A;

	}
	ComplexMatrix_T PrecodingStrategy9::Step(ComplexMatrix_T X)
	{
		return this->_P_inv * X;
	}


}