#pragma once

#include "stdafx.h"

namespace PrecodingCalculationLib {

	extern "C" class PrecodingStrategy
	{
	protected:
		ComplexMatrix_T HToA(ComplexMatrix_T);

	public:
		PrecodingStrategy() {}
		~PrecodingStrategy() {}

		virtual void Init(const ComplexMatrix_T) = 0;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) = 0;
	};


	extern "C" class PrecodingStrategy1 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _P_inv;

	public:
		void Init(const ComplexMatrix_T);
		ComplexMatrix_T Step(ComplexMatrix_T);
	};
	extern "C" class PrecodingStrategy2 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _P_inv;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy3 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _P_inv;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy4 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _P_inv;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy5 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _L;
		ComplexMatrix_T _U;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy6 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _L;
		ComplexMatrix_T _U;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy7 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _Q;
		ComplexMatrix_T _R;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy8 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _A;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};
	extern "C" class PrecodingStrategy9 : public PrecodingStrategy
	{
	protected:
		ComplexMatrix_T _P_inv;

	public:
		virtual void Init(const ComplexMatrix_T) override;
		virtual ComplexMatrix_T Step(ComplexMatrix_T) override;
	};

}

