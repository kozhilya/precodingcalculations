#pragma once

#include "stdafx.h"


namespace PrecodingCalculationLib {

	extern "C" class PrecodingMatrxAlgorithms
	{
	public:
		/// <summary>
		/// Выполняет LU-разложение методом Гаусса.
		/// </summary>
		/// <exception cref="PrecodingStrategysNotSquareException">Если матрица A не квадратная.</exception>
		/// <exception cref="PrecodingStrategysUnitaryException">Если матрица A вырождена.</exception>
		/// <param name="A">Исходная матрица.</param>
		/// <param name="L">Возвращаемая матрица разложения L (нижне-треугольная).</param>
		/// <param name="U">Возвращаемая матрица разложения U (вернхне-треугольная).</param>
		static void LU_Gauss(const ComplexMatrix_T A, ComplexMatrix_T& L, ComplexMatrix_T& U);

		/// <summary>
		/// Выполняет LU-разложение методом Жордана.
		/// </summary>
		/// <exception cref="PrecodingStrategysNotSquareException">Если матрица A не квадратная.</exception>
		/// <exception cref="PrecodingStrategysUnitaryException">Если матрица A вырождена.</exception>
		/// <param name="A">Исходная матрица.</param>
		/// <param name="L">Возвращаемая матрица разложения L (нижне-треугольная).</param>
		/// <param name="U">Возвращаемая матрица разложения U (вернхне-треугольная).</param>
		static void LU_Jordan(const ComplexMatrix_T A, ComplexMatrix_T& L, ComplexMatrix_T& U);

		/// <summary>
		/// Выполняет QR-разложение методом преобразований Хаусхолдера.
		/// </summary>
		/// <exception cref="PrecodingStrategysNotSquareException">Если матрица A не квадратная.</exception>
		/// <param name="A">Исходная матрица.</param>
		/// <param name="Q">Возвращаемая матрица разложения Q (ортогональная).</param>
		/// <param name="R">Возвращаемая матрица разложения R (вернхне-треугольная).</param>
		static void QR_Householder(const ComplexMatrix_T A, ComplexMatrix_T& Q, ComplexMatrix_T& R);

		/// <summary>
		/// Решает систему линейных алгебраических уравнений AX = B методом Гаусса.
		/// </summary>
		/// <exception cref="PrecodingStrategysDimensionMissmatchException">Если матрицы несоразмеры для выполения алгоритма.</exception>
		/// <exception cref="PrecodingStrategysUnitaryException">Если матрица A вырождена.</exception>
		/// <param name="A">Основная матрица СЛАУ A.</param>
		/// <param name="B">Матрица свободных членов B.</param>
		/// <param name="X">Возвращаемая искомая матрица X.</param>
		static void Solve_Gauss(const ComplexMatrix_T A, const ComplexMatrix_T B, ComplexMatrix_T& X);

		/// <summary>
		/// Решает систему линейных алгебраических уравнений AX = B методом наименьших квадратов.
		/// </summary>
		/// <param name="A">Основная матрица СЛАУ A.</param>
		/// <param name="B">Матрица свободных членов B.</param>
		/// <param name="X">Возвращаемая искомая матрица X.</param>
		static void Solve_LS(const ComplexMatrix_T A, const ComplexMatrix_T B, ComplexMatrix_T& X);

		/// <summary>
		/// Решает систему линейных алгебраических уравнений AX = B после LU-разложения матрицы A.
		/// </summary>
		/// <exception cref="PrecodingStrategysDimensionMissmatchException">Если матрицы несоразмеры для выполения алгоритма.</exception>
		/// <param name="L">Матрица разложения L (нижне-треугольная).</param>
		/// <param name="U">Матрица разложения U (вернхне-треугольная).</param>
		/// <param name="B">Матрица свободных членов B.</param>
		/// <param name="X">Возвращаемая искомая матрица X.</param>
		static void Solve_LU(const ComplexMatrix_T L, const ComplexMatrix_T U, const ComplexMatrix_T B, ComplexMatrix_T& X);

		/// <summary>
		/// Решает систему линейных алгебраических уравнений AX = B после QR-разложения матрицы A.
		/// </summary>
		/// <exception cref="PrecodingStrategysDimensionMissmatchException">Если матрицы несоразмеры для выполения алгоритма.</exception>
		/// <param name="Q">Матрица разложения Q (ортогональная).</param>
		/// <param name="R">Матрица разложения R (вернхне-треугольная).</param>
		/// <param name="B">Матрица свободных членов B.</param>
		/// <param name="X">Возвращаемая искомая матрица X.</param>
		static void Solve_QR(const ComplexMatrix_T Q, const ComplexMatrix_T R, const ComplexMatrix_T B, ComplexMatrix_T& X);

		/// <summary>
		/// Обращает матрицу элиминативным методом после LU-разложения.
		/// </summary>
		/// <exception cref="PrecodingStrategysDimensionMissmatchException">Если матрицы несоразмеры для выполения алгоритма.</exception>
		/// <param name="L">Матрица разложения L (нижне-треугольная).</param>
		/// <param name="U">Матрица разложения U (вернхне-треугольная).</param>
		/// <param name="X">Возвращаемая искомая обратная матрица.</param>
		static void Inverse_LU_Eliminative(const ComplexMatrix_T L, const ComplexMatrix_T U, ComplexMatrix_T& X);
	};

}