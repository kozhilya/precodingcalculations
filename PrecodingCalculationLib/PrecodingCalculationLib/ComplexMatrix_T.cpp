#pragma once

#include "stdafx.h"
#include "ComplexMatrix_T.h"

namespace PrecodingCalculationLib {

	ComplexMatrix_T::ComplexMatrix_T(unsigned int N, unsigned int M)
	{
		this->_N = N;
		this->_M = M;

		this->_alloc();
	}

	bool ComplexMatrix_T::IsSquare() const
	{
		return this->_N == this->_M;
	}

	bool ComplexMatrix_T::IsVector() const
	{
		return (this->_N == 1) || (this->_M == 1);
	}

	ComplexMatrix_T::~ComplexMatrix_T()
	{
		// delete[] this->_data;
	}

	ComplexMatrix_T ComplexMatrix_T::Eye(unsigned int N)
	{
		ComplexMatrix_T E(N, N);
		for (unsigned int i = 0; i < N; i++) {
			E(i, i) = Complex_T(1, 0);
		}
		return E;
	}

	void ComplexMatrix_T::_alloc()
	{
		unsigned int len = this->_N * this->_M;

		this->_data = new Complex_T[len];

		for (unsigned int i = 0; i < len; i++) {
			this->_data[i] = Complex_T();
		}
	}

	unsigned int ComplexMatrix_T::_getIndex(unsigned int i, unsigned int j) const
	{
		if ((i > this->_N) || (j > this->_M)) {
			throw new MatrixOutOfRangeException();
		}

		return this->_M * i + j;
	}

	unsigned int ComplexMatrix_T::GetN() const
	{
		return this->_N;
	}
	unsigned int ComplexMatrix_T::GetM() const
	{
		return this->_M;
	}

	Complex_T ComplexMatrix_T::GetValue(unsigned int i, unsigned int j)  const
	{
		return this->_data[this->_getIndex(i, j)];
	}
	void ComplexMatrix_T::SetValue(unsigned int i, unsigned int j, const Complex_T value)
	{
		this->_data[this->_getIndex(i, j)] = value;
	}
	Complex_T& ComplexMatrix_T::operator() (unsigned int i, unsigned int j)
	{
		return this->_data[this->_getIndex(i, j)];
	}
	Complex_T ComplexMatrix_T::operator() (unsigned int i, unsigned int j) const
	{
		return this->_data[this->_getIndex(i, j)];
	}

	ComplexMatrix_T ComplexMatrix_T::Transpose() const {
		unsigned int i, j;
		ComplexMatrix_T result(this->GetM(), this->GetN());

		for (i = 0; i < this->GetM(); i++) {
			for (j = 0; j < this->GetN(); j++) {
				result.SetValue(i, j, this->GetValue(j, i));
			}
		}

		return result;
	}
	ComplexMatrix_T ComplexMatrix_T::Conjugate() const {
		unsigned int i, j;
		ComplexMatrix_T result(this->GetM(), this->GetN());

		for (i = 0; i < this->GetM(); i++) {
			for (j = 0; j < this->GetN(); j++) {
				result.SetValue(i, j, this->GetValue(j, i).Conjugate());
			}
		}

		return result;
	}

	Complex_T ComplexMatrix_T::Norm() const
	{
		if (!this->IsVector())
		{
			throw new MatrixNotAVectorException();
		}

		ComplexMatrix_T A = *this;
		unsigned int i;
		Complex_T result(0, 0);
		
		if (this->_N == 1)
		{
			A = this->Transpose();
		}

		for (i = 0; i < A._N; i++)
		{
			Complex_T q = A(i, 0);
			q = q * q;
			result = result + q;
		}

		return result.Sqrt();
	}

	ComplexMatrix_T ComplexMatrix_T::SubMatrix(unsigned int row, unsigned int col, unsigned int height, unsigned int width) const
	{
		if (row + height > this->_N) {
			height = this->_N - row;
		}
		if (col + width > this->_M) {
			width = this->_M - col;
		}

		ComplexMatrix_T result(height, width);
		unsigned int i, j;

		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				result(i, j) = this->GetValue(row + i, col + j);
			}
		}

		return result;
	}

	std::ostream& operator<<(std::ostream& os, const ComplexMatrix_T& a)
	{
		unsigned int i, j;

		os << "Matrix (" << std::endl;
		for (i = 0; i < a.GetN(); i++) {
			os << "    ";
			for (j = 0; j < a.GetM(); j++) {
				os << a(i, j) << "  ";
			}
			os << std::endl;
		}
		os << ")" << std::endl;

		return os;
	}


	ComplexMatrix_T operator+(const ComplexMatrix_T matrix) {
		unsigned int i, j;
		ComplexMatrix_T result(matrix.GetN(), matrix.GetM());

		for (i = 0; i < matrix.GetN(); i++) {
			for (j = 0; j < matrix.GetM(); j++) {
				result.SetValue(i, j, matrix.GetValue(i, j));
			}
		}

		return result;
	}
	ComplexMatrix_T operator-(const ComplexMatrix_T matrix) {
		unsigned int i, j;
		ComplexMatrix_T result(matrix.GetN(), matrix.GetM());

		for (i = 0; i < matrix.GetN(); i++) {
			for (j = 0; j < matrix.GetM(); j++) {
				result.SetValue(i, j, -matrix.GetValue(i, j));
			}
		}

		return result;
	}

	ComplexMatrix_T operator+(const ComplexMatrix_T left_matrix, const ComplexMatrix_T right_matrix)
	{
		unsigned int i, j,
			aN = left_matrix.GetN(),
			aM = left_matrix.GetM(),
			bN = right_matrix.GetN(),
			bM = right_matrix.GetM();

		if ((aN != bN) || (aM != bM)) {
			throw new MatrixDimensionMissmatchException();
		}

		ComplexMatrix_T result(aN, aM);

		for (i = 0; i < aN; i++) {
			for (j = 0; j < aM; j++) {
				result.SetValue(i, j, left_matrix.GetValue(i, j) + right_matrix.GetValue(i, j));
			}
		}

		return result;
	}
	ComplexMatrix_T operator-(const ComplexMatrix_T left_matrix, const ComplexMatrix_T right_matrix)
	{
		return left_matrix + (-right_matrix);
	}
	ComplexMatrix_T operator*(const ComplexMatrix_T left_matrix, const ComplexMatrix_T right_matrix)
	{
		unsigned int i, j, k,
			aN = left_matrix.GetN(),
			aM = left_matrix.GetM(),
			bN = right_matrix.GetN(),
			bM = right_matrix.GetM();

		if (aM != bN) {
			throw new MatrixDimensionMissmatchException();
		}

		ComplexMatrix_T result(aN, bM);

		for (i = 0; i < aN; i++) {
			for (j = 0; j < bM; j++) {
				for (k = 0; k < aM; k++) {
					result(i, j) = result(i, j) + left_matrix(i, k) * right_matrix(k, j);
				}
			}
		}

		return result;
	}
	ComplexMatrix_T operator*(const double number, const ComplexMatrix_T matrix)
	{
		unsigned int i, j, k,
			N = matrix.GetN(),
			M = matrix.GetM();

		ComplexMatrix_T result(N, M);

		for (i = 0; i < N; i++) {
			for (j = 0; j < M; j++) {
				result(i, j) = matrix(i, j) * number;
			}
		}

		return result;
	}
	ComplexMatrix_T operator*(const ComplexMatrix_T matrix, const Complex_T number)
	{
		unsigned int i, j, k,
			N = matrix.GetN(),
			M = matrix.GetM();

		ComplexMatrix_T result(N, M);

		for (i = 0; i < N; i++) {
			for (j = 0; j < M; j++) {
				result(i, j) = matrix(i, j) * number;
			}
		}

		return result;
	}
	ComplexMatrix_T operator/(const ComplexMatrix_T matrix, const Complex_T number)
	{
		unsigned int i, j, k,
			N = matrix.GetN(),
			M = matrix.GetM();

		ComplexMatrix_T result(N, M);

		for (i = 0; i < N; i++) {
			for (j = 0; j < M; j++) {
				result(i, j) = matrix(i, j) / number;
			}
		}

		return result;
	}
}