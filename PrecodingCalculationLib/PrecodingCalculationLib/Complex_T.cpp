#pragma once

#include "stdafx.h"
#include "Complex_T.h"

namespace PrecodingCalculationLib {

	Complex_T::Complex_T(double _Re, double _Im)
	{
		this->Re = _Re;
		this->Im = _Im;
	}

	double Complex_T::Abs() const
	{
		return sqrt(this->Re * this->Re + this->Im * this->Im);
	}
	double Complex_T::Angle() const
	{
		if (this->Re == 0) {
			return 0;
		}
		else {
			return atan(this->Im / this->Re);
		}
	}

	Complex_T Complex_T::Conjugate() const
	{
		return Complex_T(this->Re, -this->Im);
	}

	Complex_T Complex_T::Sqrt() const
	{
		double sq = sqrt(Re * Re + Im * Im), fim = (Im < 0) ? -1 : 1;

		return Complex_T(sqrt((sq + Re) / 2), sqrt((sq - Re) / 2) * fim);
	}

	std::string Complex_T::ToString() const
	{
		std::stringstream ss;

		if (this->Im != 0)
		{
			ss << "(" << this->Re << " + " << this->Im << "i)";
		}
		else
		{
			ss << this->Re;
		}

		return ss.str();
	}


	Complex_T::~Complex_T()
	{
	}

	Complex_T Complex_T::Random()
	{
		double
			R = ((double)rand() / RAND_MAX) * 2 - 1,
			I = ((double)rand() / RAND_MAX) * 2 - 1;

		return Complex_T(R, I);
	}

	Complex_T Complex_T::Random(double abs)
	{
		double
			Phi = ((double)rand() / RAND_MAX) * 2 * M_PI;

		return Complex_T(abs * cos(Phi), abs * sin(Phi));
	}



	// Globals
	bool operator==(const Complex_T a, const Complex_T b)
	{
		return (a.Re == b.Re) && (a.Im == b.Im);
	}
	bool operator!=(const Complex_T a, const Complex_T b)
	{
		return !(a == b);
	}

	Complex_T operator+(const Complex_T a)
	{
		return Complex_T(a.Re, a.Im);
	}
	Complex_T operator-(const Complex_T a)
	{
		return Complex_T(-a.Re, -a.Im);
	}

	Complex_T operator+(const Complex_T a, const Complex_T b)
	{
		Complex_T result(a.Re + b.Re, a.Im + b.Im);
		return result;
	}
	Complex_T operator-(const Complex_T a, const Complex_T b)
	{
		Complex_T result(a.Re - b.Re, a.Im - b.Im);
		return result;
	}
	Complex_T operator*(const Complex_T a, const Complex_T b)
	{
		Complex_T result(a.Re * b.Re - a.Im * b.Im, a.Im * b.Re + a.Re * b.Im);

		return result;
	}
	Complex_T operator/(const Complex_T a, const Complex_T b)
	{
		double dividor = b.Re * b.Re + b.Im * b.Im;

		if (dividor == 0) {
			throw new ComplexDivisionByZeroException();
		}

		Complex_T result((a.Re * b.Re + a.Im * b.Im) / dividor, (a.Im * b.Re - a.Re * b.Im) / dividor);

		return result;
	}

	std::ostream& operator<<(std::ostream& os, const Complex_T& comp)
	{
		return os << comp.ToString();
	}

}