﻿// stdafx.h: включаемый файл для стандартных системных включаемых файлов
// или включаемых файлов для конкретного проекта, которые часто используются, но
// нечасто изменяются
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Исключите редко используемые компоненты из заголовков Windows
#define _USE_MATH_DEFINES


// установите здесь ссылки на дополнительные заголовки, требующиеся для программы

#include <windows.h>

#include <iostream>
#include <cmath>
#include <sstream>

#include "Complex_T.h"
#include "ComplexMatrix_T.h"
#include "Exceptions.h"
#include "Algs.h"
#include "Strategies.h"